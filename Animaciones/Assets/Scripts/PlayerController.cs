using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float velocidad;
    public Animator animatorRun;
    bool isrun = false;
    bool cayendo;
    public float jumpHeight;
    public bool isJumping = false; // this doesn't need to be public
    private Rigidbody2D _rigidBody2D;
    public float tiempocaida;
    public bool isgrounded;


    // Start is called before the first frame update
    void Start()
    {
        animatorRun = GetComponent<Animator>();
    }
    private void Awake()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        animatorRun.SetBool("isgrounded", isgrounded);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, 0.5f, LayerMask.GetMask("ground"));
        if ((hit.collider != null) && (hit.collider.CompareTag("ground")))
        {
            isgrounded = true;

        }
        else
        {
            isgrounded = false;
        }
        isrun = false;
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(velocidad * Vector3.left);
            GetComponent<SpriteRenderer>().flipX = true;
            isrun = true;
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(velocidad * Vector3.right);
            GetComponent<SpriteRenderer>().flipX = false;
            isrun = true;
        }

        if (Input.GetKey(KeyCode.Space))
        {
            if (isgrounded == true)
            {
                _rigidBody2D.velocity = new Vector2(_rigidBody2D.velocity.x, jumpHeight);
                isgrounded = false;

            }
        }
        if (_rigidBody2D.velocity.y > 0)
        {
            cayendo = false;
            
        }
        if (_rigidBody2D.velocity.y < 0)
        {
            cayendo = true;
        }
        animatorRun.SetBool("cayendo", cayendo);


        animatorRun.SetBool("isrun", isrun);

    }
    
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("pinchos"))
        {
            Destroy(gameObject);
            GameManager.EstaMuerto = true;
        }
        



    }
    
        
    
}
