using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool EstaMuerto;
    public GameObject gameOver;
    
    // Start is called before the first frame update
    void Start()
    {
        gameOver.SetActive(false);
        EstaMuerto = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (EstaMuerto == true)
        {
            muerte();
        }
    }
    public void muerte()
    {
        gameOver.SetActive(true);
    }
    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
